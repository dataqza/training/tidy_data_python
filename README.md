# Tidy with Python

Repository to help teach users coming from Excel to use (i) Pandas and (ii) better practice for data management by reading in messy data, procesing into Tidy Data standard and then performing basic analysis. Aims to take a top down approach by introducing pandas and using it's functionality to introduce Python.

Setup

``` bash
python3 -m venv venv
source venv/bin/acticate
pip install -U pip
pip install -r requirements
```

## Data Semantics

A dataset is a collection of values. Every value belongs to:

1.  A variable: contains all values that measure the same underlying attribute across units
1.  An observation: contains all values measured on the same unit across attributes. There may be multiple levels of observation

## Tidy Data Definition

A dataset is messy or tidy depending on how columns, rows and tables are matched up with variables, observations and observational units.

In the Tidy Data Standard:

1. Variables => Columns (colvars): each variable forms a column.
2. Observations => Rows: each observation forms a row.
3. Observational Units => Tables: each type of observational unit forms a table.

Style guide: fixed variables should come first, followed by measured variables, each ordered so that related variables are contiguous.

## Cleaning Tools

* Melting/Unpivoting (columns into rows)
* Casting/Unstack (rows to columns)
* String manipulation
* Splitting
* Regular expressions

## Example

### Dataset in Messy Format

| Name | Age | Treatment A | Treatment B |
| ------ | ------ | ------ | ------ |
| John Smith | 21 |  - | 2 | 
| Jane Doe | 34 | 16 | 11 |
| Mary Johnson | 25 | 3 | 1 |  

According to the data semantics defined above, the data contains 18 values representing:

* 3 variables: participant name, age, treatment, result
* 9 observations: 3 for age and 6 for treatment results
* 2 observational units: the personal details for each participant and the measurements for respective treatments for each participant. 

NB: Missing values are important, and must be dealt with within the context of the study design.

### Dataset in Tidy Format

After melting `Treatment A` and `Treatment B` columns to the variable `Treatment` which records the treatment type value for each observation we get the following tidy data table  

| Name | Treatment | Result |
| ------ | ------ | ------ |
| John Smith | a | - |
| Jane Doe | a | 16 |
| Mary Johnson | a | 3 |
| John Smith | b | 2 |
| Jane Doe | b | 11 |
| Mary Johnson | b | 1 |

## Common Messy Patterns

* Column headers are values, not variable names.
* Multiple variables are stored in one column.
* Variables are stored in both rows and columns.
* Multiple types of observational units are stored in the same table.
* A single observational unit is stored in multiple tables

## Tidy Data vs Messy Data

Tidy and Messy data are both useful. Choose the format that makes
analysis easier

Advantages of tidy data:

* Because tidy data is a standard way of structuring a dataset, it easy for an analyst or a computer to extract needed variables.
* Many data analysis operations, including all aggregation functions, involve all of the values in a variable.

Messy data can provide:

* Easier data capturing
* Presentation
* Efficient storage for completely crossed designs, and can lead to extremely efficient computation if desired operations can be expressed
as matrix operations.

## Verbs of Data Manipulation

A general rule of thumb for which tidy data makes analysis easy:

* It is easier to describe functional relationships between variables (e.g., z is a linear combination of x and y, density is the ratio of weight to volume) than between
rows, and
* It is easier to make comparisons between groups of observations (e.g., average of group a vs. average of group b) than between groups of columns.

As a result the four "verbs" of data manipulation are made easier:

* Filter: sub-setting or removing observations based on some condition.
* Transform: adding or modifying variables. These modifications can involve either a single variable (e.g., log-transformation), or multiple variables (e.g., computing density from weight and volume).
* Aggregate: collapsing multiple values into a single value (e.g., by summing or taking means). 
* Sort: changing the order of observations.

## References

- Data files at http://stat405.had.co.nz/
- Wickham, H. (2014). Tidy Data. Journal of Statistical Software, 59(10), 123. https://doi.org/10.18637/jss.v059.i10
- [Getting started tutorials — pandas 1.0.3 documentation](https://pandas.pydata.org/docs/getting_started/intro_tutorials/index.html)
