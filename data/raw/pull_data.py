import pandas as pd
import urllib.request
import csv
import io

def process_text_file(url):
    lines = [line.split("\t") for line in urllib.request.urlopen(url).read().decode('utf-8').split("\n")]

    df = pd.DataFrame(data=lines[1:-1], columns=lines[0])
    df = df.applymap(lambda x: x.replace('"', ''))
    df.columns = [ i.replace('"', '') for i in df.columns.values]

    return df


def process_csv_file(url, encoding="utf-8"):
    return pd.read_csv(io.StringIO(urllib.request.urlopen(url).read().decode(encoding)))


if __name__ == "__main__":
    df = process_text_file("http://stat405.had.co.nz/data/weather.txt")
    df.to_csv('weather_raw.csv')

    df = process_text_file("http://stat405.had.co.nz/data/pew.txt")
    df.to_csv('pew_raw.csv')

    df = process_csv_file("http://stat405.had.co.nz/data/tb.csv")
    df.to_csv("tb_raw.csv")

    df = process_csv_file("http://stat405.had.co.nz/data/billboard.csv", encoding='ISO-8859-1')
    df.to_csv("billboard_raw.csv")

